<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\loginController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/create/table', [AdminController::class, 'testtable']);
Route::get('/user/login', [loginController::class, 'login'])->name('user.login');

//for category controller
Route::get('/category', [CategoryController::class, 'category'])->name('category');
Route::get('/create/category', [CategoryController::class, 'createCategory'])->name('category.create');
Route::get('/subCategory', [CategoryController::class, 'subCategory'])->name('subCategory');
Route::get('/create/subCategory', [CategoryController::class, 'createSubCategory'])->name('subcategory.create');

//for product controller
Route::get('/allproduct', [ProductController::class,'products'])->name('products');