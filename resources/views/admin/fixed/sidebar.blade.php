<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <ul>
                <div class="logo"><a href="index.html">
                        <span>GroceryOrzo</span></a></div>
                <li class="label">Main</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> Dashboard <span
                            class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="{{route('user.login')}}">Dashboard 1</a></li>
                    </ul>
                </li>

                <li class="label">Manage</li>
                <li><a class="sidebar-sub-toggle"><i class="fa fa-list-ol" aria-hidden="true"></i>
                    Category <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                <ul>
                    <li><a href="{{route('category')}}">Main Category</a></li>
                    <li><a href="{{route('subCategory')}}">Sub Category</a></li>
                </ul>
            </li>
            <li><a class="sidebar-sub-toggle"><i class="fas fa-carrot"></i> Products <span
                class="sidebar-collapse-icon ti-angle-down"></span></a>
        <ul>
            <li><a href="{{route('products')}}"">All Products</a></li>
            <li><a href="chart-morris.html">Add New Products</a></li>
            <li><a href="chartjs.html">Processing Order</a></li>
            <li><a href="chartist.html">Completed Order</a></li>
            <li><a href="chart-peity.html">Declined Order</a></li>
        </ul>
    </li>
                <li><a class="sidebar-sub-toggle"><i class="fas fa-cart-plus"></i> Orders <span
                            class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="chart-flot.html">All Orders</a></li>
                        <li><a href="chart-morris.html">Pending Orders</a></li>
                        <li><a href="chartjs.html">Processing Orders</a></li>
                        <li><a href="chartist.html">Completed Orders</a></li>
                        <li><a href="chart-peity.html">Declined Orders</a></li>
                    </ul>
                </li>
                
                <li><a class=""><i class="fa fa-upload" aria-hidden="true"></i>

                        Bulk Product Upload <span class="sidebar-collapse-icon fa fa-chevron-right"></span></a>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="fa fa-users"></i> Customers <span
                            class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="chart-flot.html">Customers List</a></li>
                    </ul>
                </li>
                
                <li><a class="sidebar-sub-toggle"><i class="fa fa-user"></i> Roles <span
                            class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="chart-flot.html">Staffs</a></li>
                        <li><a href="chart-flot.html">Admin</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="fa fa-line-chart" aria-hidden="true"></i>
                    Analytics <span
                            class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="chart-flot.html">Customers</a></li>
                        <li><a href="chart-flot.html">Sales</a></li>
                        <li><a href="chart-flot.html">Revenue</a></li>
                    </ul>
                </li>


                <li><a><i class="ti-close"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</div>
