<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function category()
    {
        return view('admin.pages.category.category');
    }
    public function createCategory()
    {
        return view('admin.pages.category.create_category');
    }
    public function subCategory()
    {
        return view('admin.pages.category.subCategory');
    }
    public function createSubCategory(){
        return view('admin.pages.category.create-sub-category');
    }
}
